@extends('layouts.applcp')

@section('content')
<div class="card card-custom">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card-header">
        <h3 class="card-title">
            Hotel Details
        </h3>
    </div>
    <!--begin::Form-->
    <form class="form" method="POST" action="{{route('hotelDetails.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Room No </label>
                <input type="text" class="form-control form-control-lg @error('room_no') is-invalid @enderror " placeholder="Room No" name="room_no" id="room_no"/>
                @error('room_no')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleSelectl">Category</label>
                <select class="form-control form-control-lg @error('category') is-invalid @enderror" id="category" name="category">
                    <option value="Single">Single</option>
                    <option value="Double">Double</option>
                    <option value="King">King</option>
                </select>
                @error('category')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

            </div>
            <div class="form-group">
                <label>Cost </label>
                <input type="text" class="form-control form-control-lg @error('cost') is-invalid @enderror" id="cost" placeholder="Cost" name="cost" />
                @error('cost')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>No of Guest </label>
                <input type="text" class="form-control form-control-lg @error('title') is-invalid @enderror" placeholder="No Of Guest" name="guest_no" id="guest_no" />
                @error('guest_no')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>Room Size </label>
                <input type="text" class="form-control form-control-lg @error('room_size') is-invalid @enderror " placeholder="Room Size in Sqft" name="room_size" id="room_size" />
                @error('room_size')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>Photo</label>
                <input type="file" class="form-control form-control-lg @error('imagepath') is-invalid @enderror " placeholder="Photo" name="imagepath" id="imagepath" />
                @error('imagepath')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="card-footer">
                <button type="Submit" class="btn btn-success mr-2">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
    </form>
    <!--end::Form-->
</div>
@endsection